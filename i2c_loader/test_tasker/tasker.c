#include "tasker.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#include "tasker_debug.h"

volatile uint16_t counter = 0;
const uint16_t ram_end = RAMEND;


uint8_t	gl_saved_registers[32];
uint16_t gl_pc;
uint8_t	gl_stack;
uint16_t gl_stack_pointer;


//TODO: move me to scheduler
//ISR(TIMER0_OVF_vect)
void tasker_schedule(uint16_t caller_pc)
{
	//int pc = get_caller_pc();
	//TTask *current = tasker_tasks_current();
	//TTask *next = tasker_tasks_next();
	//asm_restore_task(next);

	/*if (TIFR0 & (1 << TOV0) && (counter++) == 0xfff)
	{
		counter = 0;
	}*/
}

void tasker_init()
{
	tasker_debug_print("Init TASKER\n");
}

void tasker_run()
{
	TCCR0B = (1 << CS00);
	TCNT0 = 0xff;
	TIMSK0 = 1 << TOIE0;
	
	sei();
}
