#include "tasker_config.h"
#include "tasker_debug.h"
#include "tasker_tasks.h"
#include "tasker.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../../../simulavr/src/simulavr_info.h"
SIMINFO_DEVICE("atmega1284p");
SIMINFO_CPUFREQUENCY(F_CPU);
SIMINFO_SERIAL_IN("D0", "-", BAUD);
SIMINFO_SERIAL_OUT("D1", "-", BAUD);

volatile uint16_t counter;


int task1()
{
	while(1)
		counter++;
}

int task2()
{
	while(1)
	{
		cli();
		tasker_debug_print("counter = %.4x\n", counter);
		sei();
	}
}

int main()
{
	uint16_t i;

	tasker_debug_init(0);
	tasker_init();

	/*struct tasker_i2c i2c_dev[2];

	tasker_module_load("debug", "uart", 0, 115200, 'n', 8);
	tasker_module_load("i2c");
	tasker_i2c_open(&i2c_dev[0], 0, TASKER_I2C_MASTER);
	tasker_i2c_open(&i2c_dev[1], 1, TASKER_I2C_SLAVE, 0x42);

	tasker_module_list();
	*/
	
	// Create 2 tasks
	TTask* tasks[2];
	tasks[0] = tasker_create_task();
	tasks[1] = tasker_create_task();

	tasks[0]->run_ptr = task1;
	tasks[1]->run_ptr = task2;

	for (i = 0; i < 2; i++)
	{
		if (!tasker_start_task(tasks[i]))
			tasker_debug_print("Failed to start task %u\n", i);
	}

	tasker_run();

	/*while(1)
	{
		int j = 42;
		for (j = 0; j < 0xfff; j++);
		cli();
		tasker_debug_print("counter = %.4x\n", counter);
		sei();
	}*/
	
	return 0;
}
