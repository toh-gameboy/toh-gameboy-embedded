#include <avr/io.h>
#include <avr/interrupt.h>

#include "tasker_debug.h"
#include "tasker_tasks.h"

uint16_t next_task = 0;
volatile uint8_t saved_registers[32];
volatile uint16_t saved_pc;

extern void save_registers(uint8_t *registers);
extern uint16_t get_caller_pc(void);

/*uint16_t get_caller_pc()
{
	uint16_t *sp = (uint16_t*)(*((uint16_t*)0x5d));
	return *(sp - 5);
}*/

uint16_t _tasker_fork()
{
	/*gl_pc = get_caller_pc();
	tasker_debug_print("table address = %.4x\n", (uint16_t)saved_registers);
	save_registers(saved_registers);

	int i;

	tasker_debug_print("Registers = {\n");
	for (i = 0; i < 32; i++)
		tasker_debug_print("	%.1x,\n", saved_registers[i]);
	tasker_debug_print("}\n");

	tasker_debug_print("Saving PC (%.4x) and forking...\n", gl_pc);

	return gl_pc;*/
	return 0;
}

TTask _tasks[32];

TTask* tasker_create_task(int (*run_func)(void))
{
	//TODO: return the next available task pointer.
	//	NULL if no more task
	_tasks[next_task].run_ptr = run_func;
	_tasks[next_task].task_status = Created;

	return &(_tasks[next_task++]);
}

int tasker_start_task(TTask* task)
{
	task->task_status = Starting;
	return 1;
}

