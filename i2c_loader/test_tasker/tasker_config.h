#ifndef TASKER_CONFIG_H
#define TASKER_CONFIG_H

#define F_CPU	8000000
#define BAUD	9600UL

#define MASK(n)		(1 << (n))

#endif //TASKER_CONFIG_H

