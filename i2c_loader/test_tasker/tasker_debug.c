#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>

#include "tasker_config.h"
#include "tasker_debug.h"

char printf_buffer[128] = {'a', 'b', 'c', 'd', 'e'};

void tasker_debug_init(int port)
{
	//TODO: initialize depending on port
	// Init serial port.
	UCSR0A = 0;
	UBRR0L = (F_CPU / (16*BAUD)) - 1;

	UCSR0B = MASK(RXEN0) | MASK(TXEN0);
	UCSR0C = MASK(UCSZ01) | MASK(UCSZ00);
}

int tasker_debug_print(const char* format, ...)
{
	int i;
	va_list ap;
	va_start(ap, format);

	int ret = vsnprintf(printf_buffer, 1024, format, ap);

	va_end(ap);

	for (i = 0; i < ret; i++)
	{
		while (!(UCSR0A & MASK(UDRE0)));
		UDR0 = printf_buffer[i];
	}

	return ret;
}


