#include "cartidge.h"

#include <string.h>

#ifndef __AVR_ATmega1284__
#define __AVR_ATmega1284__
#endif

#include <avr/io.h>

unsigned char nintendo_logo[] = {
   0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
   0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
   0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
};


void set_address(uint16_t address)
{
   PORTA = address & 0xff;
   PORTA = (address >> 8) & 0xff;
}

void set_data(uint8_t data)
{
   // Port C and D must be output
   DDRD |= 0x03;
   DDRC |= 0x18;

   // Setting value
   PORTD |= data & 0xfc;
   PORTC |= (data & 0x3) << 3;
}

uint8_t get_data()
{
   // Port C and D must be input
   DDRD &= ~0x03;
   DDRC &= ~0x18;

   // Getting value
   return (PIND & 0xfc) + ((PINC >> 3) & 0x3);
}

int32_t cartidge_read_bytes(uint16_t address, uint16_t length, void *data)
{
   unsigned int i;

   for (i = 0; i < length; i++)
   {
      set_address(address);

      __asm("nop");
      __asm("nop");
      __asm("nop");

      ((uint8_t*)data)[i] = get_data();
   }

   return i;
}

void cartidge_write_bytes(uint16_t address, uint16_t length, void *data)
{
   unsigned int i;
   for (i = 0; i < length; i++)
   {
      set_address(address);

      __asm("nop");
      __asm("nop");
      __asm("nop");

      set_data(((uint8_t*)data)[i]);

      __asm("nop");
      __asm("nop");
      __asm("nop");
   }
}

void cartidge_write_byte(uint16_t address, uint8_t data)
{
      set_address(address);

      __asm("nop");
      __asm("nop");
      __asm("nop");

      set_data(data);

      __asm("nop");
      __asm("nop");
      __asm("nop");
}

bool cartidge_read_bank(struct cart_reader *reader, uint8_t bank)
{
   // Init I2C for bank data transfer
   // Read bytes and send them to i2c. Note that it should be done for every byte, don't read all then send all.
   return false;
}

bool cartridge_init(struct cart_reader *reader)
{
   //Should check checksum too

   //Check logo
   char nintendo_logo_check[48];
   cartidge_read_bytes(0x104, 48, nintendo_logo_check);
   if (memcmp(nintendo_logo_check, nintendo_logo, 48))
      return false;

   reader->current_bank = 1;
   CircularBufferInit(&reader->buffer);

   // Get Game Name
   cartidge_read_bytes(0x134, 16, reader->game_name);

   // Get bank type
   uint8_t bank_type = 0;
   cartidge_read_bytes(0x147, 1, &bank_type);
   reader->bank_type = (enum mbc_type)bank_type;

   return true;
}

bool cartridge_switch_bank(struct cart_reader *reader, uint8_t new_bank)
{
   //TODO:Use a switch to manage different mbc behaviors.

   if (new_bank == 0 || new_bank == 0x20 || new_bank == 0x40 || new_bank == 0x60)
      reader->current_bank = new_bank + 1;
   else
      reader->current_bank = new_bank;

   cartidge_write_byte(0x2000, reader->current_bank & 0x1f);
   cartidge_write_byte(0x4000, (reader->current_bank >> 5) & 0x60);

   return true;
}
