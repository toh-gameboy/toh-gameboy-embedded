#ifndef TASKER_H
#define TASKER_H

#include "tasker_tasks.h"
#include "tasker_modules.h"
#include "tasker_interrupts.h"
#include "tasker_mutex.h"

void tasker_init(void);
void tasker_run(void);

#endif
