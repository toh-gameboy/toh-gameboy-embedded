#ifndef TASKER_MUTEX_H
#define TASKER_MUTEX_H

typedef struct
{
	uint16_t count;
} TMutex;

TMutex takser_mutex_create(uint16_t count);
int tasker_mutex_lock(TMutex* mutex);
int tasker_mutex_unlock(TMutex* mutex);
int tasker_mutex_try_unlock(TMutex* mutex);

#endif
