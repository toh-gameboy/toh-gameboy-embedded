/**
 * Low level cartidge access functions.
 * The API is in beta and could evolve, depending on different needs regarding the Cartidge access
 */

#ifndef CARTIDGE_H
#define CARTIDGE_H

#include <stdint.h>
#include <stdbool.h>

#include "circularbuffer.h"

enum mbc_type
{
   mbc_type_0                 = 0,
   mbc_type_0_ram             = 8,
   mbc_type_0_ram_bat         = 9,
   mbc_type_1                 = 1,
   mbc_type_1_ram             = 2,
   mbc_type_1_ram_bat         = 3,
   mbc_type_2                 = 5,
   mbc_type_2_ram_bat         = 6,
   mbc_type_mmm01             = 0xb,
   mbc_type_mmm01_ram         = 0xc,
   mbc_type_mmm01_ram_bat     = 0xd,
   mbc_type_3_time_bat        = 0xf,
   mbc_type_3_time_bat_ram    = 0x10,
   mbc_type_3                 = 0x11,
   mbc_type_3_ram             = 0x12,
   mbc_type_3_ram_bat         = 0x13,
   mbc_type_5                 = 0x19,
   mbc_type_5_ram             = 0x1a,
   mbc_type_5_ram_bat         = 0x1b,
   mbc_type_5_rumble          = 0x1c,
   mbc_type_5_ram_rumble      = 0x1d,
   mbc_type_5_ram_bat_rumble  = 0x1e,
   mbc_type_6_ram_bat         = 0x20,
   mbc_type_7_ram_bat_acc     = 0x22,
   mbc_type_cam               = 0xfc,
   mbc_type_bandai            = 0xfd,
   mbc_type_HuC3              = 0xfe,
   mbc_type_HuC1_ram_bat      = 0xff
};

typedef struct cart_reader
{
   char game_name[16];
   uint16_t current_bank;
   circular_buffer buffer;
   enum mbc_type bank_type;
} cart_reader;

bool cartridge_init(cart_reader *reader);

/**
 * @brief cartidge_read_bytes Read n bytes from the specified address
 *        and store them in data.
 * @param address The address where the read must begin
 * @param length The number of bytes to read
 * @param data the buffer in which data will be stored
 * @return the number of read bytes. Negative if an error occured
 *          (TODO:error codes to be specified)
 *
 * The function will return the number of read bytes.
 * If length gets out of the address space, of X bytes, only length - X bytes
 * are read and that value is returned.
 *
 */
int32_t cartidge_read_bytes(uint16_t address, uint16_t length, void* data);

/**
 * @brief cartidge_write_bytes
 * @param address
 * @param length
 * @param data
 */
void cartidge_write_bytes(uint16_t address, uint16_t length, void* data);

/**
 * @brief cartidge_read_bank
 * @param reader
 * @param bank
 * @return
 */
bool cartidge_read_bank(cart_reader *reader, uint8_t bank);

#endif
