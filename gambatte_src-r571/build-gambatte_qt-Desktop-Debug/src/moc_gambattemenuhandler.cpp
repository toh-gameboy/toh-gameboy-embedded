/****************************************************************************
** Meta object code from reading C++ file 'gambattemenuhandler.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gambatte_qt/src/gambattemenuhandler.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gambattemenuhandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_FrameRateAdjuster_t {
    QByteArrayData data[8];
    char stringdata[98];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FrameRateAdjuster_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FrameRateAdjuster_t qt_meta_stringdata_FrameRateAdjuster = {
    {
QT_MOC_LITERAL(0, 0, 17), // "FrameRateAdjuster"
QT_MOC_LITERAL(1, 18, 11), // "setDisabled"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 8), // "disabled"
QT_MOC_LITERAL(4, 40, 12), // "decFrameRate"
QT_MOC_LITERAL(5, 53, 12), // "incFrameRate"
QT_MOC_LITERAL(6, 66, 14), // "resetFrameRate"
QT_MOC_LITERAL(7, 81, 16) // "miscDialogChange"

    },
    "FrameRateAdjuster\0setDisabled\0\0disabled\0"
    "decFrameRate\0incFrameRate\0resetFrameRate\0"
    "miscDialogChange"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FrameRateAdjuster[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   39,    2, 0x0a /* Public */,
       4,    0,   42,    2, 0x0a /* Public */,
       5,    0,   43,    2, 0x0a /* Public */,
       6,    0,   44,    2, 0x0a /* Public */,
       7,    0,   45,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void FrameRateAdjuster::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FrameRateAdjuster *_t = static_cast<FrameRateAdjuster *>(_o);
        switch (_id) {
        case 0: _t->setDisabled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->decFrameRate(); break;
        case 2: _t->incFrameRate(); break;
        case 3: _t->resetFrameRate(); break;
        case 4: _t->miscDialogChange(); break;
        default: ;
        }
    }
}

const QMetaObject FrameRateAdjuster::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FrameRateAdjuster.data,
      qt_meta_data_FrameRateAdjuster,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *FrameRateAdjuster::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FrameRateAdjuster::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_FrameRateAdjuster.stringdata))
        return static_cast<void*>(const_cast< FrameRateAdjuster*>(this));
    return QObject::qt_metacast(_clname);
}

int FrameRateAdjuster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
struct qt_meta_stringdata_WindowSizeMenu_t {
    QByteArrayData data[3];
    char stringdata[26];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_WindowSizeMenu_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_WindowSizeMenu_t qt_meta_stringdata_WindowSizeMenu = {
    {
QT_MOC_LITERAL(0, 0, 14), // "WindowSizeMenu"
QT_MOC_LITERAL(1, 15, 9), // "triggered"
QT_MOC_LITERAL(2, 25, 0) // ""

    },
    "WindowSizeMenu\0triggered\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_WindowSizeMenu[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void WindowSizeMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        WindowSizeMenu *_t = static_cast<WindowSizeMenu *>(_o);
        switch (_id) {
        case 0: _t->triggered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject WindowSizeMenu::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_WindowSizeMenu.data,
      qt_meta_data_WindowSizeMenu,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *WindowSizeMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *WindowSizeMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_WindowSizeMenu.stringdata))
        return static_cast<void*>(const_cast< WindowSizeMenu*>(this));
    return QObject::qt_metacast(_clname);
}

int WindowSizeMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_GambatteMenuHandler_t {
    QByteArrayData data[35];
    char stringdata[537];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GambatteMenuHandler_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GambatteMenuHandler_t qt_meta_stringdata_GambatteMenuHandler = {
    {
QT_MOC_LITERAL(0, 0, 19), // "GambatteMenuHandler"
QT_MOC_LITERAL(1, 20, 9), // "romLoaded"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 12), // "dmgRomLoaded"
QT_MOC_LITERAL(4, 44, 4), // "open"
QT_MOC_LITERAL(5, 49, 14), // "openRecentFile"
QT_MOC_LITERAL(6, 64, 5), // "about"
QT_MOC_LITERAL(7, 70, 19), // "globalPaletteChange"
QT_MOC_LITERAL(8, 90, 16), // "romPaletteChange"
QT_MOC_LITERAL(9, 107, 17), // "videoDialogChange"
QT_MOC_LITERAL(10, 125, 17), // "soundDialogChange"
QT_MOC_LITERAL(11, 143, 16), // "miscDialogChange"
QT_MOC_LITERAL(12, 160, 17), // "cheatDialogChange"
QT_MOC_LITERAL(13, 178, 35), // "reconsiderSyncFrameRateAction..."
QT_MOC_LITERAL(14, 214, 23), // "execGlobalPaletteDialog"
QT_MOC_LITERAL(15, 238, 20), // "execRomPaletteDialog"
QT_MOC_LITERAL(16, 259, 15), // "execInputDialog"
QT_MOC_LITERAL(17, 275, 15), // "execSoundDialog"
QT_MOC_LITERAL(18, 291, 15), // "execVideoDialog"
QT_MOC_LITERAL(19, 307, 14), // "execMiscDialog"
QT_MOC_LITERAL(20, 322, 13), // "prevStateSlot"
QT_MOC_LITERAL(21, 336, 13), // "nextStateSlot"
QT_MOC_LITERAL(22, 350, 15), // "selectStateSlot"
QT_MOC_LITERAL(23, 366, 9), // "saveState"
QT_MOC_LITERAL(24, 376, 11), // "saveStateAs"
QT_MOC_LITERAL(25, 388, 9), // "loadState"
QT_MOC_LITERAL(26, 398, 13), // "loadStateFrom"
QT_MOC_LITERAL(27, 412, 5), // "reset"
QT_MOC_LITERAL(28, 418, 11), // "pauseChange"
QT_MOC_LITERAL(29, 430, 9), // "frameStep"
QT_MOC_LITERAL(30, 440, 10), // "escPressed"
QT_MOC_LITERAL(31, 451, 19), // "videoBlitterFailure"
QT_MOC_LITERAL(32, 471, 18), // "audioEngineFailure"
QT_MOC_LITERAL(33, 490, 16), // "toggleFullScreen"
QT_MOC_LITERAL(34, 507, 29) // "saveWindowSizeIfNotFullScreen"

    },
    "GambatteMenuHandler\0romLoaded\0\0"
    "dmgRomLoaded\0open\0openRecentFile\0about\0"
    "globalPaletteChange\0romPaletteChange\0"
    "videoDialogChange\0soundDialogChange\0"
    "miscDialogChange\0cheatDialogChange\0"
    "reconsiderSyncFrameRateActionEnable\0"
    "execGlobalPaletteDialog\0execRomPaletteDialog\0"
    "execInputDialog\0execSoundDialog\0"
    "execVideoDialog\0execMiscDialog\0"
    "prevStateSlot\0nextStateSlot\0selectStateSlot\0"
    "saveState\0saveStateAs\0loadState\0"
    "loadStateFrom\0reset\0pauseChange\0"
    "frameStep\0escPressed\0videoBlitterFailure\0"
    "audioEngineFailure\0toggleFullScreen\0"
    "saveWindowSizeIfNotFullScreen"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GambatteMenuHandler[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,  179,    2, 0x06 /* Public */,
       3,    1,  182,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,  185,    2, 0x08 /* Private */,
       5,    0,  186,    2, 0x08 /* Private */,
       6,    0,  187,    2, 0x08 /* Private */,
       7,    0,  188,    2, 0x08 /* Private */,
       8,    0,  189,    2, 0x08 /* Private */,
       9,    0,  190,    2, 0x08 /* Private */,
      10,    0,  191,    2, 0x08 /* Private */,
      11,    0,  192,    2, 0x08 /* Private */,
      12,    0,  193,    2, 0x08 /* Private */,
      13,    0,  194,    2, 0x08 /* Private */,
      14,    0,  195,    2, 0x08 /* Private */,
      15,    0,  196,    2, 0x08 /* Private */,
      16,    0,  197,    2, 0x08 /* Private */,
      17,    0,  198,    2, 0x08 /* Private */,
      18,    0,  199,    2, 0x08 /* Private */,
      19,    0,  200,    2, 0x08 /* Private */,
      20,    0,  201,    2, 0x08 /* Private */,
      21,    0,  202,    2, 0x08 /* Private */,
      22,    0,  203,    2, 0x08 /* Private */,
      23,    0,  204,    2, 0x08 /* Private */,
      24,    0,  205,    2, 0x08 /* Private */,
      25,    0,  206,    2, 0x08 /* Private */,
      26,    0,  207,    2, 0x08 /* Private */,
      27,    0,  208,    2, 0x08 /* Private */,
      28,    0,  209,    2, 0x08 /* Private */,
      29,    0,  210,    2, 0x08 /* Private */,
      30,    0,  211,    2, 0x08 /* Private */,
      31,    0,  212,    2, 0x08 /* Private */,
      32,    0,  213,    2, 0x08 /* Private */,
      33,    0,  214,    2, 0x08 /* Private */,
      34,    0,  215,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void GambatteMenuHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GambatteMenuHandler *_t = static_cast<GambatteMenuHandler *>(_o);
        switch (_id) {
        case 0: _t->romLoaded((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->dmgRomLoaded((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 2: _t->open(); break;
        case 3: _t->openRecentFile(); break;
        case 4: _t->about(); break;
        case 5: _t->globalPaletteChange(); break;
        case 6: _t->romPaletteChange(); break;
        case 7: _t->videoDialogChange(); break;
        case 8: _t->soundDialogChange(); break;
        case 9: _t->miscDialogChange(); break;
        case 10: _t->cheatDialogChange(); break;
        case 11: _t->reconsiderSyncFrameRateActionEnable(); break;
        case 12: _t->execGlobalPaletteDialog(); break;
        case 13: _t->execRomPaletteDialog(); break;
        case 14: _t->execInputDialog(); break;
        case 15: _t->execSoundDialog(); break;
        case 16: _t->execVideoDialog(); break;
        case 17: _t->execMiscDialog(); break;
        case 18: _t->prevStateSlot(); break;
        case 19: _t->nextStateSlot(); break;
        case 20: _t->selectStateSlot(); break;
        case 21: _t->saveState(); break;
        case 22: _t->saveStateAs(); break;
        case 23: _t->loadState(); break;
        case 24: _t->loadStateFrom(); break;
        case 25: _t->reset(); break;
        case 26: _t->pauseChange(); break;
        case 27: _t->frameStep(); break;
        case 28: _t->escPressed(); break;
        case 29: _t->videoBlitterFailure(); break;
        case 30: _t->audioEngineFailure(); break;
        case 31: _t->toggleFullScreen(); break;
        case 32: _t->saveWindowSizeIfNotFullScreen(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (GambatteMenuHandler::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteMenuHandler::romLoaded)) {
                *result = 0;
            }
        }
        {
            typedef void (GambatteMenuHandler::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteMenuHandler::dmgRomLoaded)) {
                *result = 1;
            }
        }
    }
}

const QMetaObject GambatteMenuHandler::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GambatteMenuHandler.data,
      qt_meta_data_GambatteMenuHandler,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GambatteMenuHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GambatteMenuHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GambatteMenuHandler.stringdata))
        return static_cast<void*>(const_cast< GambatteMenuHandler*>(this));
    return QObject::qt_metacast(_clname);
}

int GambatteMenuHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 33;
    }
    return _id;
}

// SIGNAL 0
void GambatteMenuHandler::romLoaded(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void GambatteMenuHandler::dmgRomLoaded(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
