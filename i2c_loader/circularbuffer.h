#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdint.h>
#include <stdbool.h>

#include "params.h"

typedef struct
{
   uint16_t frame_count;
   uint16_t capacity;

   void *buffer[CIRCULAR_BUFFER_CAPACITY];

   uint16_t tail;
   uint16_t head;
} circular_buffer;



//TODO: Un-CamelCase me !

void CircularBufferInit(volatile circular_buffer *buffer);
void CircularBufferDestroy(volatile circular_buffer *buffer);

void* CircularBufferPop(volatile circular_buffer *buffer);

void* CircularBufferPeek(volatile circular_buffer *buffer);
void* CircularBufferPeekAt(volatile circular_buffer *buffer, uint16_t position);

bool CircularBufferPut(volatile circular_buffer *buffer, void* ptr);

uint16_t CircularBufferCount(volatile circular_buffer *buffer);

bool CircularBufferIsFull(volatile circular_buffer *buffer);

#endif
