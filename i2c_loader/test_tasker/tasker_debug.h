#ifndef TASKER_DEBUG_H
#define TASKER_DEBUG_H

void tasker_debug_init(int port);
int tasker_debug_print(const char* format, ...);

#endif //TASKER_DEBUG_H
