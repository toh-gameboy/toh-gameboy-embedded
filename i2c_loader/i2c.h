#ifndef I2C_H
#define I2C_H

#include "circularbuffer.h"
#include "params.h"

#ifndef F_CPU
#error "F_CPU must be defined"
#endif

typedef enum i2c_action
{
    action_read_bank = 0
} i2c_action;

typedef struct i2c_command
{
    uint8_t action;
    uint8_t arguments[10];
    uint8_t argument_count;
} i2c_command;

bool i2c_init(uint8_t address, circular_buffer* buffer);

void i2c_write_next_byte();

unsigned int i2c_command_count();
i2c_command i2c_get_next_command();

#endif
