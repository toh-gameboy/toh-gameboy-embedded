/*
 * This simple program shows how to start with Qt-Creator, avr-g++ and avrdude
 *
 *
 *  Fuses configuration (Fuses OK (H:FF, E:DF, L:EE):
 *       Extended :      0xDF
 *       High :          0xFF
 *       Low :           0xEE
 *
 *
 * To compile the project :
 *   avr-g++ -mmcu=atmega32m1 -Os main.cpp -o output.elf
 *
 * To upload the file :
 *   avrdude -c avrispmkII -p m32m1 -P usb -U flash:w:output.elf
 *
 * To update the fuses :
 *   avrdude -c avrispmkII -p m32m1 -P usb -U flash:w:output.elf  -Ulfuse:w:0xeEE:m -Uhfuse:w:0xFF:m -Uefuse:w:0xDF:m
 *
 * Qt Creator project configuration:
 *
 *  Build steps (Custom Process Step)
 *      Command :               avr-g++
 *      Arguments :             -mmcu=atmega32m1 -Os main.cpp -o output.elf
 *      Working directory :     %{buildDir}
 *
 *  Clean steps(Custom Process Step)
 *      Command :               avrdude
 *      Arguments :             -c avrispmkII -p m32m1 -P usb -U flash:w:output.elf
 *      Working directory :     %{buildDir}
 *
 *  Run (Custom

 *
*/

#include "params.h"

#include <avr/io.h>
#include <avr/common.h>
#include <util/delay.h>

#include "cartidge.h"
#include "i2c.h"

typedef struct {
   //UBYTE[]
   uint32_t cpsr;
   uint32_t address_space;
   uint32_t address_space_size;
   //TYPE registers;
   //TYPE heap/stack;
} Task;

typedef struct {
   Task *tasks;
   uint8_t task_count;
   uint8_t current_task_index;
} Scheduler;

typedef enum {
   CardInserted         = 1,
   CardRemoved          = 1 << 1,
   CardEvent            = CardInserted | CardRemoved,
   I2CInput             = 1 << 2,
   I2CEvent             = I2CInput,
   ButtonAPushed        = 1 << 3,
   ButtonBPushed        = 1 << 4,
   ButtonSelectPushed   = 1 << 5,
   ButtonStartPushed    = 1 << 6,
   ButtonLeftPushed     = 1 << 7,
   ButtonRightPushed    = 1 << 8,
   ButtonUpPushed       = 1 << 9,
   ButtonDownPushed     = 1 << 10,
   ButtonEvent          = ButtonAPushed | ButtonBPushed | ButtonSelectPushed | ButtonStartPushed | ButtonUpPushed | ButtonLeftPushed | ButtonRightPushed | ButtonDownPushed
} EventFlags;

// TODO:Card reader will write in a circular buffer

volatile uint16_t event_flags;
volatile circular_buffer buffer;
struct cart_reader cartridge;

int main()
{
    //Init uC and peripherals
    CircularBufferInit(&buffer);
    i2c_init(42, &buffer);//FIXME: buffer should be internal.
    cartridge_init(&cartridge);
 
    while (1)
    {
        if (event_flags & I2CEvent && i2c_command_count())
        {
            struct i2c_command command = i2c_get_next_command();
            switch (command.action)
            {
            case action_read_bank:
                cartidge_read_bank(&cartridge, command.arguments[0]);
                break;
            }

        }
        /*__enable_interrupt();

        __sleep();

        __disable_interrupt();

        if (event_flags & CardEvent)
        {
           i2c_set_card_presence(event_flags & CardInserted);
        }
        if (event_flags & I2CEvent)
        {
           i2c_main_loop();
        }
        if (event_flags & ButtonEvent)
        {
           i2c_button_event(event_flags >> 3);
        }*/
    }
}
