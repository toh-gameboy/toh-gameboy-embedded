#include "circularbuffer.h"
#include <string.h>

void CircularBufferInit(volatile circular_buffer* buffer)
{
   memset(buffer, 0, sizeof(circular_buffer));
   buffer->capacity = CIRCULAR_BUFFER_CAPACITY;
}

void CircularBufferDestroy(volatile circular_buffer* buffer)
{
   buffer = buffer;
}


void* CircularBufferPop(volatile circular_buffer* buffer)
{
   if (!buffer->frame_count)
      return 0;

   void* ptr = buffer->buffer[buffer->tail];
   buffer->buffer[buffer->tail] = 0;
   buffer->tail = (buffer->tail + 1) % buffer->capacity;
   buffer->frame_count -= 1;

   return ptr;
}


void* CircularBufferPeek(volatile circular_buffer* buffer)
{
   return CircularBufferPeekAt(buffer, 0);
}


void* CircularBufferPeekAt(volatile circular_buffer* buffer, uint16_t position)
{
   if (position >= buffer->capacity)
      return 0;

   if (buffer->frame_count < position)
      return 0;

   return buffer->buffer[buffer->tail + position];
}


bool CircularBufferPut(volatile circular_buffer* buffer, void* ptr)
{
   if (buffer->frame_count == buffer->capacity)
       return false;

   buffer->buffer[buffer->head] = ptr;
   buffer->head = (buffer->head + 1) % buffer->capacity;
   buffer->frame_count += 1;

   return true;
}


uint16_t CircularBufferCount(volatile circular_buffer* buffer)
{
   return buffer->frame_count;
}


bool CircularBufferIsFull(volatile circular_buffer* buffer)
{
   return buffer->capacity == buffer->frame_count;
}
