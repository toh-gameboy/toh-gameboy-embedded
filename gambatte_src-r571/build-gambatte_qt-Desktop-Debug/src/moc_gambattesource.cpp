/****************************************************************************
** Meta object code from reading C++ file 'gambattesource.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gambatte_qt/src/gambattesource.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'gambattesource.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GambatteSource_t {
    QByteArrayData data[14];
    char stringdata[156];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GambatteSource_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GambatteSource_t qt_meta_stringdata_GambatteSource = {
    {
QT_MOC_LITERAL(0, 0, 14), // "GambatteSource"
QT_MOC_LITERAL(1, 15, 8), // "setTurbo"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 2), // "on"
QT_MOC_LITERAL(4, 28, 11), // "togglePause"
QT_MOC_LITERAL(5, 40, 9), // "frameStep"
QT_MOC_LITERAL(6, 50, 12), // "decFrameRate"
QT_MOC_LITERAL(7, 63, 12), // "incFrameRate"
QT_MOC_LITERAL(8, 76, 14), // "resetFrameRate"
QT_MOC_LITERAL(9, 91, 13), // "prevStateSlot"
QT_MOC_LITERAL(10, 105, 13), // "nextStateSlot"
QT_MOC_LITERAL(11, 119, 15), // "saveStateSignal"
QT_MOC_LITERAL(12, 135, 15), // "loadStateSignal"
QT_MOC_LITERAL(13, 151, 4) // "quit"

    },
    "GambatteSource\0setTurbo\0\0on\0togglePause\0"
    "frameStep\0decFrameRate\0incFrameRate\0"
    "resetFrameRate\0prevStateSlot\0nextStateSlot\0"
    "saveStateSignal\0loadStateSignal\0quit"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GambatteSource[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      11,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       4,    0,   72,    2, 0x06 /* Public */,
       5,    0,   73,    2, 0x06 /* Public */,
       6,    0,   74,    2, 0x06 /* Public */,
       7,    0,   75,    2, 0x06 /* Public */,
       8,    0,   76,    2, 0x06 /* Public */,
       9,    0,   77,    2, 0x06 /* Public */,
      10,    0,   78,    2, 0x06 /* Public */,
      11,    0,   79,    2, 0x06 /* Public */,
      12,    0,   80,    2, 0x06 /* Public */,
      13,    0,   81,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Bool,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void GambatteSource::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GambatteSource *_t = static_cast<GambatteSource *>(_o);
        switch (_id) {
        case 0: _t->setTurbo((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 1: _t->togglePause(); break;
        case 2: _t->frameStep(); break;
        case 3: _t->decFrameRate(); break;
        case 4: _t->incFrameRate(); break;
        case 5: _t->resetFrameRate(); break;
        case 6: _t->prevStateSlot(); break;
        case 7: _t->nextStateSlot(); break;
        case 8: _t->saveStateSignal(); break;
        case 9: _t->loadStateSignal(); break;
        case 10: _t->quit(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (GambatteSource::*_t)(bool );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::setTurbo)) {
                *result = 0;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::togglePause)) {
                *result = 1;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::frameStep)) {
                *result = 2;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::decFrameRate)) {
                *result = 3;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::incFrameRate)) {
                *result = 4;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::resetFrameRate)) {
                *result = 5;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::prevStateSlot)) {
                *result = 6;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::nextStateSlot)) {
                *result = 7;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::saveStateSignal)) {
                *result = 8;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::loadStateSignal)) {
                *result = 9;
            }
        }
        {
            typedef void (GambatteSource::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&GambatteSource::quit)) {
                *result = 10;
            }
        }
    }
}

const QMetaObject GambatteSource::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_GambatteSource.data,
      qt_meta_data_GambatteSource,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GambatteSource::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GambatteSource::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GambatteSource.stringdata))
        return static_cast<void*>(const_cast< GambatteSource*>(this));
    if (!strcmp(_clname, "MediaSource"))
        return static_cast< MediaSource*>(const_cast< GambatteSource*>(this));
    return QObject::qt_metacast(_clname);
}

int GambatteSource::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void GambatteSource::setTurbo(bool _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void GambatteSource::togglePause()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void GambatteSource::frameStep()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void GambatteSource::decFrameRate()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}

// SIGNAL 4
void GambatteSource::incFrameRate()
{
    QMetaObject::activate(this, &staticMetaObject, 4, Q_NULLPTR);
}

// SIGNAL 5
void GambatteSource::resetFrameRate()
{
    QMetaObject::activate(this, &staticMetaObject, 5, Q_NULLPTR);
}

// SIGNAL 6
void GambatteSource::prevStateSlot()
{
    QMetaObject::activate(this, &staticMetaObject, 6, Q_NULLPTR);
}

// SIGNAL 7
void GambatteSource::nextStateSlot()
{
    QMetaObject::activate(this, &staticMetaObject, 7, Q_NULLPTR);
}

// SIGNAL 8
void GambatteSource::saveStateSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 8, Q_NULLPTR);
}

// SIGNAL 9
void GambatteSource::loadStateSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 9, Q_NULLPTR);
}

// SIGNAL 10
void GambatteSource::quit()
{
    QMetaObject::activate(this, &staticMetaObject, 10, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
