#include "i2c.h"

#ifndef __AVR_ATmega1284__
#define __AVR_ATmega1284__
#endif

#include <avr/io.h>

bool i2c_init(uint8_t slave_address, circular_buffer* buffer)
{
    if (slave_address & (1 << 7))
        return false;

    TWAR = slave_address;

    return true;
}


void i2c_wait_master()
{
    TWCR |= (1 << TWEA) | (1 << TWEN);
}


void i2c_process_event()
{

}
