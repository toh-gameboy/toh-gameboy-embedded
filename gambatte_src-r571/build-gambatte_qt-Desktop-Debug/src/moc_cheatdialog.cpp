/****************************************************************************
** Meta object code from reading C++ file 'cheatdialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../gambatte_qt/src/cheatdialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cheatdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_GetCheatInput_t {
    QByteArrayData data[3];
    char stringdata[30];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_GetCheatInput_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_GetCheatInput_t qt_meta_stringdata_GetCheatInput = {
    {
QT_MOC_LITERAL(0, 0, 13), // "GetCheatInput"
QT_MOC_LITERAL(1, 14, 14), // "codeTextEdited"
QT_MOC_LITERAL(2, 29, 0) // ""

    },
    "GetCheatInput\0codeTextEdited\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_GetCheatInput[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   19,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,

       0        // eod
};

void GetCheatInput::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        GetCheatInput *_t = static_cast<GetCheatInput *>(_o);
        switch (_id) {
        case 0: _t->codeTextEdited(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject GetCheatInput::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_GetCheatInput.data,
      qt_meta_data_GetCheatInput,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *GetCheatInput::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *GetCheatInput::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_GetCheatInput.stringdata))
        return static_cast<void*>(const_cast< GetCheatInput*>(this));
    return QDialog::qt_metacast(_clname);
}

int GetCheatInput::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 1)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 1;
    }
    return _id;
}
struct qt_meta_stringdata_CheatDialog_t {
    QByteArrayData data[9];
    char stringdata[83];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CheatDialog_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CheatDialog_t qt_meta_stringdata_CheatDialog = {
    {
QT_MOC_LITERAL(0, 0, 11), // "CheatDialog"
QT_MOC_LITERAL(1, 12, 6), // "accept"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 6), // "reject"
QT_MOC_LITERAL(4, 27, 8), // "addCheat"
QT_MOC_LITERAL(5, 36, 9), // "editCheat"
QT_MOC_LITERAL(6, 46, 11), // "removeCheat"
QT_MOC_LITERAL(7, 58, 16), // "selectionChanged"
QT_MOC_LITERAL(8, 75, 7) // "current"

    },
    "CheatDialog\0accept\0\0reject\0addCheat\0"
    "editCheat\0removeCheat\0selectionChanged\0"
    "current"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CheatDialog[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x0a /* Public */,
       3,    0,   45,    2, 0x0a /* Public */,
       4,    0,   46,    2, 0x08 /* Private */,
       5,    0,   47,    2, 0x08 /* Private */,
       6,    0,   48,    2, 0x08 /* Private */,
       7,    1,   49,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QModelIndex,    8,

       0        // eod
};

void CheatDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CheatDialog *_t = static_cast<CheatDialog *>(_o);
        switch (_id) {
        case 0: _t->accept(); break;
        case 1: _t->reject(); break;
        case 2: _t->addCheat(); break;
        case 3: _t->editCheat(); break;
        case 4: _t->removeCheat(); break;
        case 5: _t->selectionChanged((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject CheatDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_CheatDialog.data,
      qt_meta_data_CheatDialog,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *CheatDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CheatDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_CheatDialog.stringdata))
        return static_cast<void*>(const_cast< CheatDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int CheatDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
