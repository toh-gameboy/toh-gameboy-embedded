
.global get_next_pc
.global get_caller_pc
.global save_registers

.extern ram_end
.extern gl_pc
.extern counter

; Return the PC value for the next instruction after the call to this function
get_next_pc:
	pop r25
	pop r24
	push r24
	push r25
	ret

; Return the PC value for the next instruction after the caller function returns.
get_caller_pc:
	push    r28
	push    r29
	push    r30
	push    r31
	push    r0
	in      r30, 0x3d       ; 61
	in      r31, 0x3e       ; 62
	adiw    r30, 0xc        ; 12
	ld      r24, Z
	ldd     r25, Z+1         ; 0x01
	;std     Y+2, r25        ; 0x02
	;std     Y+1, r24        ; 0x01
	;ldd     r24, Y+1        ; 0x01
	;ldd     r25, Y+2        ; 0x02
	;sbiw    r24, 0x10        ; 10
	;movw    r30, r24
	;ld      r24, Z
	;ldd     r25, Z+1        ; 0x01
	pop     r0
	pop     r31
	pop     r30
	pop     r29
	pop     r28
	ret
	
; Save the device register.
; The argument to the function is a pointer to an array of REG_SIZE.
; The size of the array must be large enough to store all registers.
; It returns nothing: 
;  void save_registers(REG_SIZE registers[REG_COUNT]);
save_registers:
	push r30
	push r31
	movw r30, r24
	std Z+0, r0
	std Z+1, r1
	std Z+2, r2
	std Z+3, r3
	std Z+4, r4
	std Z+5, r5
	std Z+6, r6
	std Z+7, r7
	std Z+8, r8
	std Z+9, r9
	std Z+10, r10
	std Z+11, r11
	std Z+12, r12
	std Z+13, r13
	std Z+14, r14
	std Z+15, r15
	std Z+16, r16
	std Z+17, r17
	std Z+18, r18
	std Z+19, r19
	std Z+20, r20
	std Z+21, r21
	std Z+22, r22
	std Z+23, r23
	std Z+24, r24
	std Z+25, r25
	std Z+26, r26
	std Z+27, r27
	std Z+28, r28
	std Z+29, r29
	pop r31
	pop r30
	push r28
	push r29
	movw r28, r24
	std Y+30, r30
	std Y+31, r31
	pop r29
	pop r28
	ret

.global __vector_18
__vector_18:
	push	r1
	push	r0
	lds	r0, 0x005F
	push	r0
	eor	r1, r1
	push	r28
	push	r29
	in	r28, 0x3d	; 61
	in	r29, 0x3e	; 62
	push	r24
	push	r25
	call	get_caller_pc
        sts	counter, r24
        sts	counter+1, r25
;TODO: save stack
;TODO: save registers
	pop	r25
	pop	r24
	pop	r29
	pop	r28
	pop	r0
	sts	0x005F, r0
	pop	r0
	pop	r1
	reti
