#ifndef TASKER_TASKS_H
#define TASKER_TASKS_H

#include <stdint.h>

#define TASK_MAX_COUNT		32

typedef enum {
	Created,
	Starting,
	Running,
	Stopped,
	Blocked,
	Terminated
} Status;

// Should only contain what is common to all devices.
// Each device will have to declare a struct which contains a Task* field (a la linux)
typedef struct {
	uint16_t task_id;
	uint32_t regs[32];
	uint32_t* stack;
	uint32_t pc;
	Status task_status;
	int (*run_ptr)(void);
} TTask;

//uint16_t _tasker_fork();


void tasker_init();
TTask* tasker_create_task();
int tasker_start_task(TTask*);

#endif
